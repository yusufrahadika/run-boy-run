var level, velocity, numBush, bushes, trees, numTree, bushImage, score, life, gameOver, zombieKilled,
    zombies, numZombie, hearts, animation, knight, restartButton, pauseButton, paused, pauseButton;

// get canvas
var canvas = document.getElementById("game_canvas");
canvas.width = 1024;
canvas.height = 480;
canvas.addEventListener("mousedown", tap);

// knight sprite sheet
var knightImage = new Image();
knightImage.src = "images/character/knight_run.png";

start();

function start() {
    // init state
    level = 1;
    velocity = 1;
    numBush = 4;
    bushes = [];
    trees = [];
    numTree = 3;
    bushImage;
    score = 0;
    life = 3;
    gameOver = "";
    zombies = [];
    numZombie = 3;
    hearts = [];
    restartButton = null;
    paused = false;
    zombieKilled = 0;

    spawnPause();

    // knight sprite
    knight = sprite({
        context: canvas.getContext("2d"),
        w: 1740,
        h: 210,
        img: knightImage,
        numberOfFrame: 10,
        tickPerFrame: 5,
        x: canvas.width * 2,
        y: canvas.height - 210
    });
    knight.x = canvas.width + Math.random() * (canvas.width - knight.getFrameWidth() * knight.scaleRatio);

    // bush
    for (let i = 0; i < numTree; i++) {
        spawnTree();
    }

    // bush
    for (let i = 0; i < numBush; i++) {
        spawnBush();
    }

    // zombie
    for (let i = 0; i < numZombie; i++) {
        spawnZombie();
    }

    for (let i = 0; i < life; i++) {
        spawnHeart();
    }

    gameLoop();
}

function spawnPause() {
    let pauseWidth = 76;
    pauseButton = sprite({
        context: canvas.getContext("2d"),
        w: pauseWidth,
        h: 32,
        img: new Image(),
        x: (canvas.width - pauseWidth) / 2,
        y: 20
    });
    pauseButton.img.src = 'images/button/btnPause.png';
}

function gameLoop() {
    animation = window.requestAnimationFrame(gameLoop);
    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);

    hearts.forEach(function (heart) {
        heart.render();
    });

    // tree
    for (let i = 0; i < trees.length; i++) {
        let tree = trees[i];
        tree.update();
        tree.x += level * velocity / 2;
        tree.render();

        if (tree.x > canvas.width + 282) {
            tree.x = -300 - Math.floor(Math.random() * 3 + 1);
        }
    }

    // knight
    knight.update();
    knight.x -= level * velocity;
    knight.render();

    if (knight.x < -1 * knight.xRange) {
        knight.x = canvas.width + Math.random() * (canvas.width - knight.getFrameWidth() * knight.scaleRatio);
    }

    // bush
    for (let i = 0; i < bushes.length; i++) {
        let bush = bushes[i];
        bush.update();
        bush.x += level * velocity / 2;
        bush.render();

        if (bush.x > canvas.width + 65) {
            bush.x = -80 - Math.floor(Math.random() * 3 + 1);
        }
    }

    drawHud();

    // zombie
    for (let i = 0; i < zombies.length; i++) {
        let zombie = zombies[i];
        zombie.update();
        zombie.x -= level * velocity;
        zombie.render();

        if (zombie.x < -1 * zombie.xRange) {
            zombie.x = canvas.width + Math.random() * (canvas.width - zombie.getFrameWidth() * zombie.scaleRatio);
            reduceLife();
            if (score > level * 2) {
                level++;
            }
        }
    }

    if (restartButton) {
        restartButton.render();
    }
}

function reduceLife() {
    life--;
    hearts.pop();
    if (life === 0) {
        gameOver = "Game Over";
        var restartImage = new Image();
        restartImage.src = "images/button/btnRestart.png";

        let restartWidth = 128;

        restartButton = sprite({
            context: canvas.getContext("2d"),
            w: restartWidth,
            h: 64,
            img: restartImage,
            numberOfFrame: 1,
            tickPerFrame: 1,
            x: (canvas.width - restartWidth) / 2,
            y: 240
        });
        setTimeout(() => cancelAnimationFrame(animation), 100);
    }
}

function sprite(options) {
    var that = {},
        frameIndex = 0,
        tickCount = 0,
        tickPerFrame = options.tickPerFrame || 0,
        numberOfFrame = options.numberOfFrame || 1;

    that.context = options.context;
    that.w = options.w;
    that.h = options.h;
    that.img = options.img;
    that.x = options.x;
    that.y = options.y;
    that.xRange = that.w / numberOfFrame;
    that.scaleRatio = 1;

    that.update = function () {
        tickCount += 1;
        if (tickCount > tickPerFrame) {
            tickCount = 0;
            if (frameIndex < numberOfFrame - 1) {
                frameIndex += 1;
            } else {
                frameIndex = 0;
            }
        }
    };

    that.render = function () {
        that.context.drawImage(
            that.img,
            frameIndex * that.w / numberOfFrame,
            0,
            that.w / numberOfFrame,
            that.h,
            that.x,
            that.y,
            that.w / numberOfFrame,
            that.h
        );
    };

    that.getFrameWidth = function () {
        return that.w / numberOfFrame;
    }

    return that;
}

function spawnHeart() {
    var heartIndex,
        heartImage = new Image();
    heartImage.src = "images/heart/heart.png";

    heartIndex = hearts.length;
    hearts[heartIndex] = sprite({
        context: canvas.getContext("2d"),
        w: 50,
        h: 40,
        img: heartImage,
        numberOfFrame: 1,
        tickPerFrame: 1,
        x: 80 + heartIndex * 50,
        y: 20
    });
}

function spawnZombie() {
    var zombieIndex,
        zombieImage;
    zombieImage = new Image();
    zombieIndex = zombies.length;
    zombies[zombieIndex] = sprite({
        context: canvas.getContext("2d"),
        w: 1740,
        h: 210,
        img: zombieImage,
        numberOfFrame: 10,
        tickPerFrame: 5
    });

    if ((zombieIndex % 2) == 1) {
        zombieImage.src = "images/character/zombie_female_run.png";
    } else {
        zombieImage.src = "images/character/zombie_run.png";
    }
    let zombie = zombies[zombieIndex];
    zombie.x = canvas.width + Math.random() * (canvas.width - zombie.getFrameWidth() * zombie.scaleRatio);
    zombie.y = canvas.height - 210;
    zombie.scaleRatio = Math.random() * 0.5 + 0.5;
}

function spawnBush() {
    var bushIndex,
        bushImage;
    bushImage = new Image();
    bushIndex = bushes.length;
    bushes[bushIndex] = sprite({
        context: canvas.getContext("2d"),
        img: bushImage,
        w: 0,
        h: 0,
        x: 0,
        y: 0,
        numberOfFrame: 1,
        tickPerFrame: 1
    });

    let bush = bushes[bushIndex];
    bush.x = 0 + Math.random() * (canvas.width - bush.getFrameWidth() * bush.scaleRatio);
    if (bushIndex == 0) {
        bush.w = 133;
        bush.h = 66;
        bush.y = canvas.height - 62;
    } else if (bushIndex == 1) {
        bush.w = 73;
        bush.h = 47;
        bush.y = canvas.height - 45;
    } else if (bushIndex == 2) {
        bush.w = 54;
        bush.h = 55;
        bush.y = canvas.height - 53;
    } else if (bushIndex == 3) {
        bush.w = 53;
        bush.h = 76;
        bush.y = canvas.height - 74;
    }
    bush.scaleRatio = Math.random() * 0.5 + 0.5;
    bushImage.src = `images/bush/bush${bushIndex}.png`;
}

function spawnTree() {
    var treeIndex,
        treeImage;
    treeImage = new Image();
    treeIndex = trees.length;
    trees[treeIndex] = sprite({
        context: canvas.getContext("2d"),
        img: treeImage,
        w: 0,
        h: 0,
        x: 0,
        y: 0,
        numberOfFrame: 1,
        tickPerFrame: 1
    });

    let tree = trees[treeIndex];
    tree.x = 0 + Math.random() * (canvas.width - tree.getFrameWidth() * tree.scaleRatio);
    tree.w = 282;
    tree.h = 301;
    tree.y = canvas.height / 2 - 64;
    tree.scaleRatio = Math.random() * 0.5 + 0.5;
    treeImage.src = `images/tree/tree${treeIndex % 2}.png`;
}

function drawHud() {
    var context = canvas.getContext("2d");
    // score
    context.font = "bold 20px Consolas";
    context.textAlign = "start";
    context.fillStyle = "white";
    context.fillText("Score: " + score, canvas.width - 275, 40);
    // level
    context.font = "bold 20px Consolas";
    context.textAlign = "start";
    context.fillStyle = "white";
    context.fillText("Level: " + level, canvas.width - 125, 40);
    // life
    context.font = "bold 20px Consolas";
    context.textAlign = "start";
    context.fillStyle = "white";
    context.fillText("Life: ", 30, 40);
    // gameover
    context.font = "bold 50px Consolas";
    context.textAlign = "center";
    context.fillStyle = "#193439";
    context.fillText(gameOver, context.canvas.width / 2, context.canvas.height / 2 - 32);
    // pause
    if (!paused) {
        pauseButton.render();
    }
}

function getElementPosition(element) {
    var parentOffset,
        pos = {
            x: element.offsetLeft,
            y: element.offsetTop
        }

    if (element.offsetParent) {
        parentOffset = getElementPosition(element.offsetParent);
        pos.x += parentOffset.x;
        pos.y += parentOffset.y;
    }

    return pos;
}

function tap(e) {
    var pos = getElementPosition(canvas),
        tapX = e.targetTouches ? e.targetTouches[0].pageX : e.pageX,
        tapY = e.targetTouches ? e.targetTouches[0].pageY : e.pageY;

    let scaledX = 640 / 1024;
    let scaledY = 360 / 480;

    if (paused) {
        gameLoop();
        gameOver = '';
        paused = false;
    } else if (life === 0 && tapX >= pos.x + (restartButton.x * scaledX) && tapX <= pos.x + (restartButton.x + restartButton.xRange) * scaledX
        && tapY >= pos.y + (restartButton.y * scaledY) - (restartButton.h * canvas.height / canvas.width) && tapY <= pos.y + (restartButton.y * scaledY)) {
        start();
    } else {
        let tapProcessed = false;

        if (!tapProcessed && tapX >= pos.x + 297 && tapX <= pos.x + 345 && tapY >= pos.y + 16 && tapY <= pos.y + 32) {
            console.log(`Paused!`);
            paused = true;
            gameOver = 'Game Paused';
            setTimeout(() => cancelAnimationFrame(animation), 100);
            tapProcessed = true;
        }

        for (let i = 0; i < zombies.length && !tapProcessed; i++) {
            let zombie = zombies[i];
            if (tapX >= pos.x + ((zombie.x + 0.2 * zombie.xRange) * scaledX) && tapX <= pos.x + ((zombie.x + 0.8 * zombie.xRange) * scaledX)
                && tapY >= pos.y + (zombie.y * scaledY) && tapY <= pos.y + ((zombie.y + zombie.h) * scaledY)) {
                console.log(`Zombie ${i} hitted!`);
                zombie.x = canvas.width + Math.random() * (canvas.width - zombie.getFrameWidth() * zombie.scaleRatio);
                tapProcessed = true;
                score++;
                zombieKilled++;
                if (zombieKilled === 5) {
                    zombieKilled = 0;
                    level++;
                }
            }
        }

        if (!tapProcessed && tapX >= pos.x + ((knight.x + 0.2 * knight.xRange) * scaledX) && tapX <= pos.x + ((knight.x + 0.8 * knight.xRange) * scaledX)
            && tapY >= pos.y + (knight.y * scaledY) && tapY <= pos.y + ((knight.y + knight.h) * scaledY)) {
            console.log(`Knight hitted!`);
            knight.x = canvas.width + Math.random() * (canvas.width - knight.getFrameWidth() * knight.scaleRatio);
            reduceLife();
            tapProcessed = true;
        }
    }
}
